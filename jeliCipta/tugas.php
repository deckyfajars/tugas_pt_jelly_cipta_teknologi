<html> 
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="assets/css/bootstrap.css"> 
	<title>TUGAS PT Jeli Cipta Teknologi</title>
	<link rel="dns-prefetch" href="//fonts.googleapis.com" />
	<link rel="dns-prefetch" href="//fonts.gstatic.com" />
	<link rel="preload" as="style" href="https://fonts.googleapis.com/css?family=Open Sans:400,500,600,700&display=swap" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open Sans:400,500,600,700&display=swap" media="print" onload="this.media='all'">
</head> 
<body data-rsssl=1> 
	<div class="container-fluid">
		<center>
			<br>
			<h2>TUGAS PT Jeli Cipta Teknologi</h2>
			<h4>Decky Fajar Sidiq</h4>
			<br>
		</center>
		<!-- GRID 1 -->
		<div class="row">

			<!-- NUMBER 1 -->
			<div class="col-md-3">
				<div class="bg-primary text-center text-white"><u><strong>NUMBER 1</strong></u></div>	
					<div class="bg-primary text-white">
						<?php
							$jumlah = 5;
							for ($i=1; $i<=$jumlah; $i++){
								for ($j=$jumlah; $j>=$i; $j--){
									echo " *";
								}
								echo "<br>";
							}
						?>
					</div>
				<br/>
			</div>


			<!-- NUMBER 2 -->
			<div class="col-md-3">
				<div class="bg-primary text-center text-white"><u><strong>NUMBER 2</strong></u></div>
					<div class="bg-primary text-white">
						<?php
							$jumlah2 = 5;
							for ($i=$jumlah2; $i>0; $i--){
								for ($j=$jumlah2; $j>=$i; $j--){
									echo " *";
								}
								echo "<br>";
							}
						?>
					</div>
				<br/>
			</div>


			<!-- NUMBER 3 -->
			<div class="col-md-3">
				<div class="bg-primary text-center text-white"><u><strong>NUMBER 3</strong></u></div>
					<div class="bg-primary text-white">
						<?php
							$jumlah3 = 6;
							for ($i=$jumlah3; $i>0; $i--){
								for ($j=$jumlah3; $j>=$i; $j--){
									echo " *";
								}
								echo "<br>";
							}
							$jumlah4 = 5;
							for ($i=1; $i<=$jumlah4; $i++){
								for ($j=$jumlah4; $j>=$i; $j--){
									echo " *";
								}
								echo "<br>";
							}
						?>
					</div>
				<br/>
			</div>


			<!-- NUMBER 4 -->
			<div class="col-md-3">
				<div class="bg-primary text-center text-white"><u><strong>NUMBER 4</strong></u></div><br>
					<input class="form control" type="number" id="no4_1" onchange="hitungFaktorial();" min="1" style="width:308px;" value="1">
					<p>Masukan Angka</p>
					<input class="form control" type="number" id="no4_2" readonly="" style="width:308px; background-color: #DCDCDC;" value="1">
					<p>Hasil Faktorial</p>
				<br/>
			</div>
		</div>
		<!-- HASIL NUMBER 4 DIBAWAH MENGGUNAKAN JAVASCRIPT -->

		<!-- GRID 2 -->
		<div class="row">

			<!-- NUMBER 5 -->
			<div class="col-md-3">
				<div class="bg-primary text-center text-white"><u><strong>NUMBER 5</strong></u></div><br>	
					<input class="form control" type="text" id="no5_1" style="width:308px;" onkeyup="ubah();" placeholder="Masukan String Menggunakan Spasi">
					<p>String</p>
					<input class="form control" type="text" id="no5_2" readonly="" style="width:308px; background-color: #DCDCDC;">
					<p>Hasil Pergantian Spasi menjadi %20</p>
				<br/>
			</div>
			<!-- HASIL NUMBER 5 DIBAWAH MENGGUNAKAN JAVASCRIPT -->


			<!-- NUMBER 6 -->
			<div class="col-md-3">
				<div class="bg-primary text-center text-white"><u><strong>NUMBER 6</strong></u></div><br>
					<input class="form control" type="number" id="no6_1" onchange="hitungFibonaci();" min="0" style="width:308px;" value="0">
					<p>Masukan Angka</p>
					<input class="form control" type="number" id="no6_2" readonly="" style="width:308px; background-color: #DCDCDC;" value="0">
					<p>Hasil Fibonaci</p>
				<br/>
			</div>
			<!-- HASIL NUMBER 6 DIBAWAH MENGGUNAKAN JAVASCRIPT -->


			<!-- NUMBER 7 -->
			<div class="col-md-3">
				<div class="bg-primary text-center text-white"><u><strong>NUMBER 7</strong></u></div><br>
					<input class="form control" type="text" id="no7_1" onkeyup="cekSpecialChar();" size="38" placeholder="Masukan String">
					<p>Masukan String</p>
					<input class="form control" type="text" id="no7_2" readonly="" size="38" value="TIDAK TERDAPAT SPECIAL CHARACTER" style="background-color: #DCDCDC;">
					<p>Hasil Check Special Character <br>Contoh Special Character <strong>@#$%^&*()-+</strong></p>
				<br/>
			</div>
			<!-- HASIL NUMBER 7 DIBAWAH MENGGUNAKAN JAVASCRIPT -->


			<!-- NUMBER 8 -->
			<div class="col-md-3">
				<div class="bg-primary text-center text-white"><u><strong>NUMBER 8</strong></u></div><br>
					<input class="form control" type="text" id="no8_1" style="width:308px; background-color: #DCDCDC;" readonly="">
					<p>Array Random</p>
					<input class="form control" type="text" id="no8_2" readonly="" style="width:308px; background-color: #DCDCDC;">
					<p>Hasil Pengurutan Array</p>
				<br/>
			</div>
		</div>
		<!-- HASIL NUMBER 8 DIBAWAH MENGGUNAKAN JAVASCRIPT -->

		<!-- GRID 3 -->
		<div class="row">

			<!-- NUMBER 9 -->
			<div class="col-md-3">
				<div class="bg-primary text-center text-white"><u><strong>NUMBER 9</strong></u></div><br>
					<input class="form control" type="text" id="no9_1" onkeyup="hapus();" style="width:308px; background-color: #DCDCDC;" readonly="" value="tugas pt jeli cipta teknologi">
					<p>String</p>
					<input class="form control" type="text" id="no9_2" onkeyup="hapus();" style="width:308px;" placeholder="Masukan 1 karakter yang akan dihapus" maxlength="1">
					<p>Hapus Karakter</p>
					<input class="form control" type="text" id="no9_3" readonly="" style="width:308px; background-color: #DCDCDC;">
					<p>Hasil Penghapusan Karakter</p>
				<br/>
			</div>
			<!-- HASIL NUMBER 9 DIBAWAH MENGGUNAKAN JAVASCRIPT -->	


			<!-- NUMBER 10 -->
			<div class="col-md-3">
				<div class="bg-primary text-center text-white"><u><strong>NUMBER 10</strong></u></div><br>
					<input class="form control" type="text" id="no10_1" readonly="" style="width:308px; background-color: #DCDCDC;">
					<p>Array Miaw</p>
					<input class="form control" type="text" id="no10_2" readonly="" style="width:308px; background-color: #DCDCDC;">
					<p>Hasil Perhitungan Miaw</p>
				<br/>
			</div>
			<!-- HASIL NUMBER 10 DIBAWAH MENGGUNAKAN JAVASCRIPT -->


			<!-- NUMBER 11 -->
			<div class="col-md-3">
				<div class="bg-primary text-center text-white"><u><strong>NUMBER 11</strong></u></div><br>
					<div class="bg-primary text-center text-white">
						<p>Saya kurang paham dengan soal ini</p>
					</div>
				<br/>
			</div>


			<!-- NUMBER 12 -->
			<div class="col-md-3">
				<div class="bg-primary text-center text-white"><u><strong>NUMBER 12</strong></u></div><br>
					<div class="bg-primary text-white">
						<?php
							$jumlah4 = 5;
							for ($i=$jumlah4; $i>0; $i--){
								for ($j=1; $j<=$i; $j++){
									echo "&nbsp";
								}
								for($k=$jumlah4; $k>=$i; $k--){
								echo " *";
							}
								echo "<br>";
							}
						?>
					</div>
				<br/>
			</div>
		</div>
	</div>

	<script type="text/javascript">

	//JAVASCRIPT NUMBER 4
	function hitungFaktorial(){
		var a = document.getElementById("no4_1").value;
		var i = 1;
		var hasil = i;
		while(i<=a){
			hasil=hasil*i;
			i++;
		}
		if (!isNaN(hasil)) {
			document.getElementById('no4_2').value=hasil;
		}
	}
	//END JAVASCRIPT NUMBER 4


	//JAVASCRIPT NUMBER 5
	function ubah(){
	    var a = document.getElementById("no5_1").value;
	    var b = a.split(' ').join('%20');
	    document.getElementById("no5_2").value=b;
 	}
	//END JAVASCRIPT NUMBER 5


	//JAVASCRIPT NUMBER 6
	function hitungFibonaci(){
		var x = document.getElementById("no6_1").value;
		var i = 0;
		var a = 1;
		var b = 1;
		while(i<x){
			document.getElementById("no6_2").value=a;
			var c = a+b;
			a = b;
			b = c;
			i++;
		}
	}
	//END JAVASCRIPT NUMBER 6


	//JAVASCRIPT NUMBER 7
	function cekSpecialChar(){
		var char = document.getElementById("no7_1");
		var cek_special = /[@#$%^&*()-+]/g;
		if(char.value.match(cek_special)){
			document.getElementById("no7_2").value= "TERDAPAT SPECIAL CHARACTER";
		} else {
			document.getElementById("no7_2").value= "TIDAK TERDAPAT SPECIAL CHARACTER";
		}
	}
	//END JAVASCRIPT NUMBER 7


	//JAVASCRIPT NUMBER 8
	var array = [8, 2, 5, 1, 5, 2, 20, 14, 8, 30, 42, 66, 254, 101];
	document.getElementById("no8_1").value=array;
	array.sort(function(x, y) {
    return x - y;
	});
	document.getElementById("no8_2").value=array;
	//END JAVASCRIPT NUMBER 8


	//JAVASCRIPT NUMBER 9
	function hapus(){
		var a = "tugas pt jeli cipta teknologi";
	    var b = document.getElementById("no9_2").value;
	    var c = a.split(b).join('');
	    document.getElementById("no9_3").value=c;
	}
	//END JAVASCRIPT NUMBER 9


	//JAVASCRIPT NUMBER 10
	var array2 = ["Miaw","miaw","miaw","miaw","miaww"];
	var i = 0;
	var jumlah = 0;
	document.getElementById("no10_1").value=array2;
	for (i=0; i<array2.length; i++){
		if (array2[i]=="miaw"){
			jumlah=jumlah+1
			document.getElementById("no10_2").value=jumlah;
		}
	}
	//END JAVASCRIPT NUMBER 10

	</script>
	<script src="assets/js/jquery.js"></script> 
	<script src="assets/js/popper.js"></script> 
	<script src="assets/js/bootstrap.js"></script>
</body> 
</html>
